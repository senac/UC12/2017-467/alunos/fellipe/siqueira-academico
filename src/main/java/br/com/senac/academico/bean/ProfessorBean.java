package br.com.senac.academico.bean;

import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Professor;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.view.ViewScoped;

@Named(value = "professorBean")
@ViewScoped
public class ProfessorBean implements Serializable {

    private Professor professor;
    private ProfessorDAO daoProfessor;

    public ProfessorBean() {
        this.professor = new Professor();
        this.daoProfessor = new ProfessorDAO();

    }

    public void salvar() {
        if (this.professor.getId() == 0) {
            daoProfessor.save(professor);
        } else {
            daoProfessor.update(professor);
        }

    }

    public String novo() {
        this.professor = new Professor();
        return "";
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

}
