package br.com.senac.academico.dao;

import br.com.senac.academico.model.Aluno;
import br.com.senac.academico.model.Curso;
import br.com.senac.academico.model.Professor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("AcademicoPU");

    public static EntityManager getEntityManager() {
        try {
            return emf.createEntityManager();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Erro ao acessar banco de dados.");
        }
    }

}
