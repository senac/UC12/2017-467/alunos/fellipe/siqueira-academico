package br.com.senac.academico.dao;


import br.com.senac.academico.model.Professor;
import java.util.List;
import javax.persistence.Query;

public class ProfessorDAO extends DAO<Professor> {

    public ProfessorDAO() {
        super(Professor.class);
    }

    public List<Professor> findByFiltro(String codigo, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Professor> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Professor a where 1=1");

        if (codigo != null && !codigo.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }

        Query query = em.createQuery(sql.toString());

        if (codigo != null && !codigo.isEmpty()) {
            query.setParameter("Id", new Long(codigo));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }

        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }

}
